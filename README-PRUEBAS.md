Crear un rol: 
POST
localhost:8085/api/rol 
{
    "name": "Prueba"
}

Traer todos los roles:
GET
localhost:8085/api/rol

Traer por un id el rol :
GET
localhost:8085/api/rol/2

Eliminar rol 
DELETE
localhost:8085/api/rol/2
