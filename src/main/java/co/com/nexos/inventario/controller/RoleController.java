package co.com.nexos.inventario.controller;

import co.com.nexos.inventario.dto.role.RoleDTO;
import co.com.nexos.inventario.dto.role.RoleDTOToRole;
import co.com.nexos.inventario.dto.role.RoleToRoleDTO;
import co.com.nexos.inventario.entity.Role;
import co.com.nexos.inventario.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*")
public class RoleController {
    @Autowired(required=true)
    private RoleService roleService;

    private RoleDTOToRole roleDTOToRole;
    private RoleToRoleDTO roleToRoleDTO;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setRoleDTOToRole(RoleDTOToRole roleDTOToRole) {
        this.roleDTOToRole = roleDTOToRole;
    }

    @Autowired
    public void setRoleToRoleDTO(RoleToRoleDTO roleToRoleDTO) {
        this.roleToRoleDTO = roleToRoleDTO;
    }

    @GetMapping("/rol")
    public ResponseEntity<?> findAll() {

        List<RoleDTO> roleDTOS = new ArrayList<>();

        for(Role role : roleService.findAll()) {
            RoleDTO roleDTO = roleToRoleDTO.convert(role);
            roleDTOS.add(roleDTO);
        }

        return new ResponseEntity<>(roleDTOS, HttpStatus.OK);
    }

    @GetMapping("/rol/{id}")
    public ResponseEntity<?> getRole(@PathVariable Long id) {

        Role role = roleService.findById(id);
        if (role == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(roleToRoleDTO.convert(role), HttpStatus.OK);
    }

    @PostMapping("/rol")
    public ResponseEntity<?> create(@RequestBody RoleDTO roleDTO) {

        if(roleDTO.getIdRole() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        roleService.save(roleDTOToRole.convert(roleDTO));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("rol/{id}")
    public ResponseEntity<?> update(@RequestBody RoleDTO roleDTO, @PathVariable Long id) {

        roleDTO.setIdRole(id);
        roleService.save(roleDTOToRole.convert(roleDTO));
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("rol/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> delete(@PathVariable Long id) {

        try {
            roleService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
