package co.com.nexos.inventario.controller;

import co.com.nexos.inventario.dto.commodity.CommodityDTO;
import co.com.nexos.inventario.dto.commodity.CommodityDTOToCommodity;
import co.com.nexos.inventario.dto.commodity.CommodityToCommodityDTO;
import co.com.nexos.inventario.entity.Commodity;
import co.com.nexos.inventario.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*")
public class CommodityController {
    @Autowired(required = true)
    private CommodityService commodityService;

    private CommodityDTOToCommodity commodityDTOToCommodity;
    private CommodityToCommodityDTO commodityToCommodityDTO;

    @Autowired
    public CommodityController(CommodityService commodityService) {
        this.commodityService = commodityService;
    }

    @Autowired
    public void setRoleDTOToRole(CommodityDTOToCommodity commodityDTOToCommodity) {
        this.commodityDTOToCommodity = commodityDTOToCommodity;
    }

    @Autowired
    public void setCommodityToCommidityDTO(CommodityToCommodityDTO commodityToCommidityDTO) {
        this.commodityToCommodityDTO = commodityToCommidityDTO;
    }

    @GetMapping("/mercancia")
    public ResponseEntity<?> findAll() {

        List<CommodityDTO> commodityDTOS = new ArrayList<>();

        for (Commodity commodity : commodityService.findAll()) {
            CommodityDTO commodityDTO = commodityToCommodityDTO.convert(commodity);
            commodityDTOS.add(commodityDTO);
        }

        return new ResponseEntity<>(commodityDTOS, HttpStatus.OK);
    }

    @GetMapping("/mercancia/{id}")
    public ResponseEntity<?> getCommodity(@PathVariable Long id) {

        Commodity commodity = commodityService.findById(id);
        if (commodity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(commodityToCommodityDTO.convert(commodity), HttpStatus.OK);
    }

    @PostMapping("/mercancia")
    public ResponseEntity<?> create(@RequestBody CommodityDTO commodityDTO) {

        if (commodityDTO.getIdCommodity() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        commodityService.save(commodityDTOToCommodity.convert(commodityDTO));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("mercancia/{id}")
    public ResponseEntity<?> update(@RequestBody CommodityDTO commodityDTO, @PathVariable Long id) {

        commodityDTO.setIdCommodity(id);
        commodityService.save(commodityDTOToCommodity.convert(commodityDTO));
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("mercancia/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> delete(@PathVariable Long id) {

        try {
            commodityService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
