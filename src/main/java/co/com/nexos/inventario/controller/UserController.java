package co.com.nexos.inventario.controller;

import co.com.nexos.inventario.dto.user.UserDTO;
import co.com.nexos.inventario.dto.user.UserDTOToUser;
import co.com.nexos.inventario.dto.user.UserToUserDTO;
import co.com.nexos.inventario.entity.User;
import co.com.nexos.inventario.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*")
public class UserController {
    @Autowired(required=true)
    private UserService userService;

    private UserDTOToUser userDTOToUser;
    private UserToUserDTO userToUserDTO;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setRoleDTOToRole(UserDTOToUser userDTOToUser) {
        this.userDTOToUser = userDTOToUser;
    }

    @Autowired
    public void setRoleToRoleDTO(UserToUserDTO userToUserDTO) {
        this.userToUserDTO = userToUserDTO;
    }

    @GetMapping("/usuario")
    public ResponseEntity<?> findAll() {

        List<UserDTO> userDTOS = new ArrayList<>();

        for(User user : userService.findAll()) {
            UserDTO userDTO = userToUserDTO.convert(user);
            userDTOS.add(userDTO);
        }

        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @GetMapping("/usuario/{id}")
    public ResponseEntity<?> getUser(@PathVariable Long id) {

        User user = userService.findById(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(userToUserDTO.convert(user), HttpStatus.OK);
    }

    @PostMapping("/usuario")
    public ResponseEntity<?> create(@RequestBody UserDTO userDTO) {

        if(userDTO.getIdUser() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        userService.save(userDTOToUser.convert(userDTO));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("usuario/{id}")
    public ResponseEntity<?> update(@RequestBody UserDTO userDTO, @PathVariable Long id) {

        userDTO.setIdUser(id);
        userService.save(userDTOToUser.convert(userDTO));
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("usuario/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> delete(@PathVariable Long id) {

        try {
            userService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
