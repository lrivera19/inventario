package co.com.nexos.inventario.service.impl;

import co.com.nexos.inventario.entity.Commodity;
import co.com.nexos.inventario.repository.CommodityRepository;
import co.com.nexos.inventario.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommodityServiceImpl implements CommodityService {
    @Autowired
    private CommodityRepository commodityRepository;

    @Override
    public List<Commodity> findAll() {
        return commodityRepository.findAll();
    }

    @Override
    public Commodity findById(Long id) {
        Optional<Commodity> result = commodityRepository.findById(id);

        Commodity commodity = null;
        if (result.isPresent()) {
            commodity = result.get();
        } else {
            throw new RuntimeException("No se encontro el ID : " + id);
        }

        return commodity;
    }

    @Override
    public Commodity save(Commodity commodity) {
        return commodityRepository.save(commodity);
    }

    @Override
    public void delete(Long id) {
        commodityRepository.deleteById(id);
    }
}
