package co.com.nexos.inventario.service;

import co.com.nexos.inventario.entity.Commodity;

import java.util.List;

public interface CommodityService {
    List<Commodity> findAll();

    Commodity findById(Long id);

    Commodity save(Commodity commodity);

    void delete(Long id);
}