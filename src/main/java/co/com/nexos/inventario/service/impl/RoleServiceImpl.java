package co.com.nexos.inventario.service.impl;

import co.com.nexos.inventario.entity.Role;
import co.com.nexos.inventario.repository.RoleRepository;
import co.com.nexos.inventario.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role save(Role role) {
        return roleRepository.save(role);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Role findById(Long id) {
        Optional<Role> result = roleRepository.findById(id);

        Role role = null;
        if (result.isPresent()) {
            role = result.get();
        } else {
            throw new RuntimeException("No se encontro el ID : " + id);
        }

        return role;
    }

    @Override
    public void delete(Long id) {
        roleRepository.deleteById(id);
    }
}
