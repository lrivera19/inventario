package co.com.nexos.inventario.service.impl;

import co.com.nexos.inventario.entity.User;
import co.com.nexos.inventario.repository.UserRepository;
import co.com.nexos.inventario.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public User findById(Long id) {
        Optional<User> result = userRepository.findById(id);

        User user = null;
        if (result.isPresent()) {
            user = result.get();
        } else {
            throw new RuntimeException("No se encontro el ID : " + id);
        }

        return user;
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }
}
