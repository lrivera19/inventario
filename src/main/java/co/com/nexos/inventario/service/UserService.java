package co.com.nexos.inventario.service;

import co.com.nexos.inventario.entity.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    User findById(Long id);

    User save(User user);

    void delete(Long id);
}
