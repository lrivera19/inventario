package co.com.nexos.inventario.service;

import co.com.nexos.inventario.entity.Role;

import java.util.List;

public interface RoleService {
    List<Role> findAll();

    Role findById(Long id);

    Role save(Role role);

    void delete(Long id);
}
