package co.com.nexos.inventario.dto.user;

import co.com.nexos.inventario.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDTO {

    public UserDTO convert(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setIdUser(user.getIdUser());
        userDTO.setName(user.getName());
        userDTO.setAge(user.getAge());
        userDTO.setRole(user.getRole());
        userDTO.setDateOfAdmission(user.getDateOfAdmission());

        return userDTO;
    }
}
