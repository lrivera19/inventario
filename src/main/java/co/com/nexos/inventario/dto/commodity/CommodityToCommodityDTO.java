package co.com.nexos.inventario.dto.commodity;

import co.com.nexos.inventario.entity.Commodity;
import org.springframework.stereotype.Component;

@Component
public class CommodityToCommodityDTO {

    public CommodityDTO convert(Commodity commodity) {
        CommodityDTO commodityDTO= new CommodityDTO();
        commodityDTO.setIdCommodity(commodity.getIdCommodity());
        commodityDTO.setName(commodity.getName());
        commodityDTO.setAmount(commodity.getAmount());
        commodityDTO.setDateOfAdmission(commodity.getDateOfAdmission());
        commodityDTO.setUser(commodity.getUser());
        commodityDTO.setModificationDate(commodity.getModificationDate());

        return commodityDTO;
    }
}
