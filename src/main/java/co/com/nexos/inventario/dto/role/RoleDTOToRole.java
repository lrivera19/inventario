package co.com.nexos.inventario.dto.role;

import co.com.nexos.inventario.entity.Role;
import co.com.nexos.inventario.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RoleDTOToRole {
    private RoleService roleService;

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public Role convert(RoleDTO roleDTO) {
        Role role = (roleDTO.getIdRole() != null ? roleService.findById(roleDTO.getIdRole()) : new Role());
        role.setName(roleDTO.getName());

        return role;
    }
}
