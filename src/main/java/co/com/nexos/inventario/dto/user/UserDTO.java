package co.com.nexos.inventario.dto.user;

import co.com.nexos.inventario.entity.Role;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class UserDTO implements Serializable {
    private static final long serialVersionUID = -1L;

    private Long idUser;
    private String name;
    private Integer age;
    private Role role;
    private Date dateOfAdmission;

    @Override
    public String toString() {
        return "UserDTO{" +
                "idUser=" + idUser +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", role=" + role +
                ", dateOfAdmission=" + dateOfAdmission +
                '}';
    }
}
