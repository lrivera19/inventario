package co.com.nexos.inventario.dto.user;

import co.com.nexos.inventario.entity.User;
import co.com.nexos.inventario.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDTOToUser {
    private UserService userService;

    @Autowired
    public void setRoleService(UserService userService) {
        this.userService = userService;
    }

    public User convert(UserDTO userDTO) {
        User user = (userDTO.getIdUser() != null ? userService.findById(userDTO.getIdUser()) : new User());
        user.setName(userDTO.getName());
        user.setAge(userDTO.getAge());
        user.setRole(userDTO.getRole());
        user.setDateOfAdmission(userDTO.getDateOfAdmission());

        return user;
    }
}
