package co.com.nexos.inventario.dto.role;

import co.com.nexos.inventario.entity.Role;
import org.springframework.stereotype.Component;

@Component
public class RoleToRoleDTO {

    public RoleDTO convert(Role role) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setIdRole(role.getIdRole());
        roleDTO.setName(role.getName());

        return roleDTO;
    }
}
