package co.com.nexos.inventario.dto.commodity;

import co.com.nexos.inventario.entity.Commodity;
import co.com.nexos.inventario.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommodityDTOToCommodity {
    private CommodityService commodityService;

    @Autowired
    public void setCommodityService(CommodityService commodityService) {
        this.commodityService = commodityService;
    }

    public Commodity convert(CommodityDTO commodityDTO) {
        Commodity commodity = (commodityDTO.getIdCommodity() != null ? commodityService.findById(commodityDTO.getIdCommodity()) : new Commodity());
        commodity.setName(commodityDTO.getName());
        commodity.setAmount(commodityDTO.getAmount());
        commodity.setDateOfAdmission(commodityDTO.getDateOfAdmission());
        commodity.setUser(commodityDTO.getUser());
        commodity.setModificationDate(commodityDTO.getModificationDate());

        return commodity;
    }
}
