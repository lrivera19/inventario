package co.com.nexos.inventario.dto.commodity;

import co.com.nexos.inventario.entity.User;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class CommodityDTO implements Serializable {
    private static final long serialVersionUID = -1L;

    private Long idCommodity;
    private String name;
    private Integer amount;
    private Date dateOfAdmission;
    private User user;
    private Date modificationDate;

    @Override
    public String toString() {
        return "CommodityDTO{" +
                "idCommodity=" + idCommodity +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", dateOfAdmission=" + dateOfAdmission +
                ", user=" + user +
                ", modificationDate=" + modificationDate +
                '}';
    }
}
