package co.com.nexos.inventario.dto.role;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class RoleDTO implements Serializable {
    private static final long serialVersionUID = -1L;

    private Long idRole;
    private String name;

    @Override
    public String toString() {
        return "RoleDTO{" +
                "idRole=" + idRole +
                ", name='" + name + '\'' +
                '}';
    }
}
