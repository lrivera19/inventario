package co.com.nexos.inventario.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "role")
@Getter
@Setter
public class Role implements Serializable {
    private static final long serialVersionUID = -1L;

    public Role(){
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long idRole;

    @NotNull(message = "Name may not be null")
    private String name;

    @OneToMany(mappedBy = "role", cascade = CascadeType.MERGE, orphanRemoval = true)
    @JsonIgnore
    private List<User> users;

    public Role(@NotNull(message = "Name may not be null") String name) {
        super();
        this.name = name;
    }
}
