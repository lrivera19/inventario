package co.com.nexos.inventario.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "commodity")
@Getter
@Setter
public class Commodity implements Serializable {
    private static final long serialVersionUID = -1L;

    public Commodity() {

    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long idCommodity;

    @NotNull(message = "Name may not be null")
    private String name;

    @NotNull(message = "Amount may not be null")
    private Integer amount;

    @Column(name = "date_of_admission")
    private Date dateOfAdmission;

    @NotNull(message = "Creation user may not be null")
    @JoinColumn(name = "creation_user_id")
    @OneToOne(fetch = FetchType.LAZY)
    private User user;

    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    public Commodity(@NotNull(message = "Name may not be null") String name, @NotNull(message = "Amount may not be null") Integer amount, Date dateOfAdmission, @NotNull(message = "Creation user may not be null") User user, User userModification, Date modificationDate) {
        super();
        this.name = name;
        this.amount = amount;
        this.dateOfAdmission = dateOfAdmission;
        this.user = user;
        this.modificationDate = modificationDate;
    }
}
