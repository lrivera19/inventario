package co.com.nexos.inventario.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "username")
@Getter
@Setter
public class User implements Serializable {
    private static final long serialVersionUID = -1L;

    public User(){
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long idUser;

    @NotNull(message = "Name may not be null")
    private String name;

    private Integer age;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;

    @Column(name = "date_of_admission")
    private Date dateOfAdmission;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnore
    private Commodity commodity;

    public User(@NotNull(message = "Name may not be null") String name, Integer age, Role role, Date dateOfAdmission) {
        super();
        this.name = name;
        this.age = age;
        this.role = role;
        this.dateOfAdmission = dateOfAdmission;
    }
}
